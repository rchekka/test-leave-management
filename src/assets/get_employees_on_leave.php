<?php 
	require '../config.php';

	$survey_json_data = array ( 

					array('leaveStartDate' => '2018-03-21',
						'leaveEndDate' => '2018-03-21',
						'name' => 'Bala Tanneeru', 
						'duration' => 0, 
						'leaveStartDay' => '', 
						'leaveEndDay' => ''
					),
					array('leaveStartDate' => '2018-03-22',
						'leaveEndDate' => '2018-03-23',
						'name' => 'Chaitanya Kunduru', 
						'duration' => 0, 
						'leaveStartDay' => '', 
						'leaveEndDay' => ''
					),
					array('leaveStartDate' => '2018-03-23',
						'leaveEndDate' => '2018-03-27',
						'name' => 'Yaswanth Jilakara', 
						'duration' => 0, 
						'leaveStartDay' => '', 
						'leaveEndDay' => ''
					));
	
	print_r(json_encode($survey_json_data, true));
	
?>