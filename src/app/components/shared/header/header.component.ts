import { Component, OnInit, AfterViewInit, HostListener, ViewChild, ElementRef   } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css'],
})

export class HeaderComponent implements OnInit, AfterViewInit {
  @ViewChild('listView') list: ElementRef;

  hideProfileOptions: boolean;
  selectedDOM;

  constructor(private element: ElementRef) { }

  ngOnInit() {
    this.hideProfileOptions = false;
    this.selectedDOM = this.list.nativeElement;
  }

  ngAfterViewInit() {
    this.selectedDOM.classList.add('d-none');
  }

  toggleProfileOptions() {
    this.hideProfileOptions = !this.hideProfileOptions;
  }

  @HostListener('document:click') clickout() {
    if (this.selectedDOM.parentNode.contains(event.target)) {
      this.selectedDOM.classList.toggle('d-none');
    } else {
      this.selectedDOM.classList.add('d-none');
    }
  }

}
