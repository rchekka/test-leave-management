import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HeaderComponent } from './header.component';
import * as $ from 'jquery';
import { element } from 'protractor';


describe('HeaderComponent', () => {
  let component: HeaderComponent;
  let fixture: ComponentFixture<HeaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HeaderComponent ]
    })
    .compileComponents();
  }));
  beforeEach(() => {
    fixture = TestBed.createComponent(HeaderComponent);
    component = fixture.componentInstance;
    component.hideProfileOptions = false;
    fixture.detectChanges();
  });

  it('checks for the height of the header', function() {
    expect(window.getComputedStyle(document.getElementsByClassName('header')[0]).height).toEqual('68px');
  });

  xit('checks for the border bottom of the header', function() {
    expect(window.getComputedStyle(document.getElementsByClassName('header')[0]).borderBottom).toEqual('0.5px solid rgb(212, 216, 230)');
  });

  it('checks for the margin of the header', function() {
    expect(window.getComputedStyle(document.getElementsByClassName('header')[0]).margin).toEqual('0px 50px');
  });

  it('test the toggle function', function() {
    $('.profile-section div:first-child').click();
    fixture.detectChanges();
    expect(component.hideProfileOptions).toEqual(true);
  });

  it('check the host listener', function() {
    // $('.profile-section div:first-child').click();
    // document.dispatchEvent(new Event('click'));
    fixture.detectChanges();
    expect(component.selectedDOM.classList).toMatch('position-relative d-none');
  });

});
