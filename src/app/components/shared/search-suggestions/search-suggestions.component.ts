import { Component, OnInit, ElementRef, HostListener, Input, Output,  ViewChild, EventEmitter} from '@angular/core';
import { Server } from 'selenium-webdriver/safari';

@Component({
  selector: 'app-search-suggestions',
  templateUrl: './search-suggestions.component.html',
  styleUrls: ['./search-suggestions.component.css']
})
export class SearchSuggestionsComponent implements OnInit {

  @ViewChild('list') list: ElementRef;
  @ViewChild('searchSuggestions') searchSuggestions: ElementRef;
  @Input() suggestionValue: any;
  @Input() suggestionOptions: any;
  @Input() selectedNotifier: any;
  @Output() selectedNotifierChange = new EventEmitter<any>();

  constructor(private element: ElementRef) { }

  searchResults: Array<string> = [];
  notifiedList: Array<string> = [];

  ngOnInit() { }

  /*
    * Name: document:click
    * Author: Rajesh
    * parameters: 0
    * Return : Nothing
    * Process: It toggles the search suggestions option when user clicks outside input.
  */
  @HostListener('document:click') clickout() {
    if (this.element.nativeElement.contains(event.target)) {
      this.list.nativeElement.classList.toggle('d-none');
    } else {
      this.list.nativeElement.classList.add('d-none');
    }
  }

  /*
    * Name: getSearchSuggestions
    * Author: Rajesh
    * parameters: 0
    * Return : Returns string that contains user input init.
    * Process: This function compares the user input with list of elements in array and returns the elements which contains user input init.
  */
  getSearchSuggestions() {
    this.searchResults = [];
    if (!this.suggestionValue) {
      this.list.nativeElement.classList.add('d-none');
      return false;
    }
    this.list.nativeElement.classList.remove('d-none');
    const dataLength = this.suggestionOptions.length;
    for (let i = 0; i < dataLength; i++) {
      // tslint:disable-next-line:prefer-const
      let userName = this.suggestionOptions[i].name;
      // tslint:disable-next-line:prefer-const
      let userEmail = this.suggestionOptions[i].email;
      // tslint:disable-next-line:prefer-const
      let searchDataWithName = userName.toLowerCase();
      // tslint:disable-next-line:prefer-const
      let searchDataWithEmail = userEmail.toLowerCase();
      // tslint:disable-next-line:prefer-const
      let searchString = this.suggestionValue.toLowerCase();
      if (searchDataWithName.includes(searchString) || searchDataWithEmail.includes(searchString)) {
        this.searchResults.push(userName);
      }
    }
  }

  /*
    * Name: selectSuggestionValue
    * Author: Rajesh
    * parameters: 1 (Selected employee)
    * Return : This function returns the selected employee
    * Process: This function takes the selected employee from search-suggestions component and
        returns it to the script file where this component is used.
  */
  selectSuggestionValue(option: string) {
    if (!this.findDuplicates(option)) {
      this.notifiedList.push(option);
      this.suggestionValue = '';
      this.searchResults = [];
      this.selectedNotifierChange.emit(this.notifiedList);
    }
  }

  /*
    * Name: findDuplicates
    * Author: Rajesh
    * parameters: 1 (Selected employee)
    * Return : True / False
    * Process: This function takes the currently selected employee and compares it with already selected employees and
        returns true if matches or returns if not matches.
  */
  findDuplicates(option) {
    return this.notifiedList.includes(option);
  }

}
