import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchSuggestionsComponent } from './search-suggestions.component';
import { AppModule } from '../../../app.module';
import * as $ from 'jquery';

describe('SearchSuggestionsComponent', () => {
  let component: SearchSuggestionsComponent;
  let fixture: ComponentFixture<SearchSuggestionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [AppModule],
      declarations: []
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchSuggestionsComponent);
    component = fixture.componentInstance;
    component.searchResults = ['Rajesh Kumar', 'Bala Tanneeru', 'Chaitanya Kunduru'];
    component.notifiedList = ['Bala Tanneeru', 'Chaitanya Kunduru'];
    fixture.detectChanges();
  });

  it('should create SearchSuggestionsComponent', () => {
    expect(component).toBeTruthy();
  });

  it('Should display suggestions on entering input', () => {
    component.suggestionOptions =  [{
      name: 'Vijaya Prabhakar Konduri',
      email: 'vkonduri@uxreactor.com'
    },
    {
        name: 'Chaitanya Kunduru',
        email: 'ckunduru@uxreactor.com'
    },
    {
        name: 'BalaNarsimha Rao Tanneeru',
        email: 'btanneeru@uxreactor.com'
    },
    {
        name: 'Raj Gopal Prasad',
        email: 'kprasad@uxreactor.com'
    }];
    component.suggestionValue = 'C';
    component.getSearchSuggestions();
    fixture.detectChanges();
    expect($('.search-suggestions-options').find('ul').find('li').eq(0).text()).toBe('Vijaya Prabhakar Konduri');
  });

  it('should run selectSuggestionValue()', () => {
    console.log(component.notifiedList);
    $('.search-suggestions-options ul li:first-child').click();
    fixture.detectChanges();
    console.log(component.notifiedList);
    expect(component).toBeTruthy();
  });
});
