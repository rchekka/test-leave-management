import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RequestLeaveModelComponent } from './request-leave-model.component';
import { AppModule } from '../../../app.module';
import * as $ from 'jquery';

describe('RequestLeaveModelComponent', () => {
  let component: RequestLeaveModelComponent;
  let fixture: ComponentFixture<RequestLeaveModelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [],
      imports: [ AppModule]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RequestLeaveModelComponent);
    component = fixture.componentInstance;
    component.selectedNotifiersList = ['Rajesh', 'Bala', 'Jimmy', 'balu'];
    fixture.detectChanges();
  });

  it('should create RequestLeaveModelComponent', () => {
    expect(component).toBeTruthy();
  });

  it('Input should display empty', () => {
    const reasonInput = $('.approver-and-reason-fields').find('.form-control').text();
    expect(reasonInput).toBe('');
  });

  it('Check if error message is displayed by clicking button with entering reason', () => {
    $('.request-leave-btn').click();
    fixture.detectChanges();
    $('.approver-and-reason-fields').find('.form-control').text('');
    $('.request-leave').find('button').click();
    fixture.detectChanges();
    const errorMes = $('.approver-and-reason-fields').find('span').text();
    expect(errorMes).toBe('Please enter the leave reason');
    expect(component.reasonErrorMessage).toBe(true);
  });

  it('Check if error message is not displayed by clicking button with the reason', () => {
    $('.approver-and-reason-fields').find('.form-control').text('Leaving Early');
    fixture.detectChanges();
    const errorMes = $('.approver-and-reason-fields').find('span').text();
    expect(errorMes).toBe('');
  });

  it('test the removeNotifier function', function() {
    $('.request-leave-btn').click();
    fixture.detectChanges();
    $('.selected-notifiers-list li:first-child .close-notifier-icon').click();
    fixture.detectChanges();
    expect(component.selectedNotifiersList).toEqual(['Bala', 'Jimmy', 'balu']);
  });

  it('test the no of leaves function', function() {
    $('.request-leave-btn').click();
    fixture.detectChanges();
    $('.leave-request-date-picker:first-child').click();
    fixture.detectChanges();
    expect(component.noOfLeaves).toEqual(1);
  });

  it('Check if error message is displayed by clicking button with entering reason', () => {
    $('.request-leave-btn').click();
    fixture.detectChanges();
    component.leaveReason = 'No comments';
    $('.request-leave').find('button').click();
    fixture.detectChanges();
    expect(component.reasonErrorMessage).toBe(false);
  });

});
