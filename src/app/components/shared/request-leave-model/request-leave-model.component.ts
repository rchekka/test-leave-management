import { Component, OnInit } from '@angular/core';
import { IMyDpOptions, IMyDateModel } from './../../../../../node_modules/angular4-datepicker/src/my-date-picker/index';

import { Constants } from './../../../constants';
import { RequestLeaveService } from '../../../services/request-leave.service';
import { LeaveDatesFormat } from '../../../leave-date-selected-data-types';


@Component({
  selector: 'app-request-leave-model',
  templateUrl: './request-leave-model.component.html',
  styleUrls: ['./request-leave-model.component.css']
})
export class RequestLeaveModelComponent implements OnInit {
  // Constants used for below variables
  userEmail: string = Constants.EMAIL;
  meridiemAm: string = Constants.AM;
  meridiemPm: string = Constants.PM;
  leaveType: string = Constants.LEAVE_TYPE;
  specialLeave: string = Constants.SPECIAL_LEAVE_TYPE;
  privilegeLeave: string = Constants.PRIVILEGE_LEAVE_TYPE;
  sickLeave: string = Constants.SICK_LEAVE_TYPE;
  userType: string = Constants.USER_TYPE;
  userTypeCeo: string = Constants. CEO_USER_TYPE;
  userTypeMember: string = Constants. MEMBER_USER_TYPE;
  privilegeLeaveBalance: number = Constants.PRIVILEGE_LEAVE_BALANCE;
  specialLeaveBalance: number = Constants.SPECIAL_LEAVE_BALANCE;
  sickLeaveBalance: number = Constants.SICK_LEAVE_BALANCE;
  leaveBalance: number = this.privilegeLeaveBalance;
  leavesTaken: number = Constants.LEAVES_TAKEN;
  informer: string = Constants.INFORMER;
  notifier: string = Constants.NOTIFIER;
  informerOptions: Object[] = Constants.LIST_OF_INFORMERS;
  notifierOptions: Object[] = Constants.LIST_OF_NOTIFIERS;

  selectedNotifiersList: Array<string>;
  startLeaveMeridiem: string;
  endLeaveMeridiem: string;
  globalEmployeesData: Array<string>;

  // Leave Start and End dates data types are inherited from "LeaveDatesFormat" class
  leaveSelectedStartDate: LeaveDatesFormat;
  leaveSelectedEndDate: LeaveDatesFormat;

  // HTML bindable variables
  noOfLeaves: number;
  leaveReason: string;
  leaveComment: string;
  leaveDatePickerOptions: any;
  leaveStartDate: any;
  leaveEndDate: any;
  requestLeaveFormData: object;
  reasonErrorMessage: boolean;
  // tslint:disable-next-line:no-inferrable-types
  startLeaveAmMeridiemStatus: boolean = false;
  // tslint:disable-next-line:no-inferrable-types
  startLeavePmMeridiemStatus: boolean = false;
  // tslint:disable-next-line:no-inferrable-types
  endLeaveAmMeridiemStatus: boolean = false;
  // tslint:disable-next-line:no-inferrable-types
  endLeavePmMeridiemStatus: boolean = false;
  // tslint:disable-next-line:no-inferrable-types
  showModal: boolean = false;

  constructor(private requestLeaveService: RequestLeaveService) { }

  ngOnInit() {
    let date: any;
    let currentYear: any;
    let currentMonth: any;
    let currentDate: any;

    // Calender Code Started
    date = new Date();
    currentYear = date.getFullYear();
    currentMonth = date.getMonth();
    currentDate = date.getDate();

    this.leaveSelectedStartDate = { year: currentYear, month: currentMonth + 1, day: currentDate };
    this.leaveSelectedEndDate = { year: currentYear, month: currentMonth + 1, day: currentDate };
    this.leaveDatePickerOptions =  {
      dateFormat: Constants.DATE_FORMAT,
      firstDayOfWeek: Constants.START_WEEK,
      inline: Constants.FALSE,
      disableWeekends: Constants.TRUE,
      openSelectorOnInputClick: Constants.TRUE,
      editableDateField: Constants.FALSE,
      showClearDateBtn: Constants.FALSE,
      disableUntil: { year: currentYear, month: currentMonth + 1, day: currentDate - 1}
    };
    // Calender Code Ended

    this.leaveStartDate = new Date();
    this.leaveEndDate = new Date();

    this.reasonErrorMessage = false;

    // tslint:disable-next-line:max-line-length
    this.noOfLeaves = Math.round(new Date(this.leaveEndDate).getTime() - new Date(this.leaveStartDate).getTime()) / Constants.ONE_DAY_TO_MILLI_SECONDS_FORMULA + 1;
  }

  /*
    * Name: onLeaveStartDateChanged(event)
    * Author: Rajesh
    * parameters: event
    * Return : nothing
    * Process: This function is used to Change the start date of leave when user selects a date.
  */
  onLeaveStartDateChanged(event) {
    this.leaveSelectedStartDate = event.date;
  }

  /*
    * Name: onLeaveEndDateChanged(event)
    * Author: Rajesh
    * parameters: event
    * Return : nothing
    * Process: This function is used to Change the end date of leave when user selects a date.
  */
  onLeaveEndDateChanged(event) {
    this.leaveSelectedEndDate = event.date;
  }

  /*
    * Name: removeNotifier
    * Author: Rajesh
    * parameters: index of notifier
    * Return : nothing
    * Process: This function is used to remove the selected notifier using index from an array of notifiers.
  */
  removeNotifier(index) {
    this.selectedNotifiersList.splice(index, 1);
  }

  /*
    * Name: caculateNoOfLeaves
    * Author: Rajesh
    * parameters: 0
    * Return : Returns Number of days selected for leave
    * Process: This function is used to calculate the number of days selected to take leave and
        returns the Number of days selected for leave.
  */
  caculateNoOfLeaves() {
    // tslint:disable-next-line:max-line-length
    this.leaveStartDate = this.leaveSelectedStartDate.year + '-' + this.leaveSelectedStartDate.month + '-' + this.leaveSelectedStartDate.day;
    this.leaveEndDate = this.leaveSelectedEndDate.year + '-' + this.leaveSelectedEndDate.month + '-' + this.leaveSelectedEndDate.day;

    // tslint:disable-next-line:max-line-length
    this.noOfLeaves = Math.round(new Date(this.leaveEndDate).getTime() - new Date(this.leaveStartDate).getTime()) / Constants.ONE_DAY_TO_MILLI_SECONDS_FORMULA + 1;
  }

  /*
    * Name: applyLeave
    * Author: Rajesh
    * parameters: 0
    * Return : True / False
    * Process: This function is used to send the requested leave data to backend using "requestEmployeeLeave" service.
        This function returs true if data sending is success or returns false if there is any error.
  */
  applyLeave() {
    if (this.leaveReason) {
      this.requestLeaveFormData = {
        leaveStartDate : new Date(this.leaveStartDate).getTime(),
        leaveEndDate : new Date(this.leaveEndDate).getTime(),
        leaveType : this.leaveType,
        startLeaveMeridiem : this.startLeaveMeridiem,
        endLeaveMeridiem : this.endLeaveMeridiem,
        informer : this.informer,
        leaveReason : this.leaveReason,
        selectedNotifiersList : this.selectedNotifiersList,
        leaveComment : this.leaveComment
      };
      this.requestLeaveService.requestEmployeeLeave(this.requestLeaveFormData).subscribe(
        (response) => {
          return true;
        },
        (error) => {
          return false;
        }
      );
      this.reasonErrorMessage = false;
    } else {
      this.reasonErrorMessage = true;
    }
  }
}
