import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DropdownComponent } from './dropdown.component';

import { FormsModule } from '@angular/forms';

import * as $ from 'jquery';

describe('DropdownComponent', () => {
  let component: DropdownComponent;
  let fixture: ComponentFixture<DropdownComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ FormsModule ],
      declarations: [ DropdownComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DropdownComponent);
    component = fixture.componentInstance;
    component.ddValue = 'vkonduri';

    component.ddOptions = [{
        value: 'vkonduri'
    }, {
        value: 'ckunduru'
    }, {
        value: 'btanneeru'
    }];
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should create dropdown list', () => {
    const optionsLength = component.ddOptions.length;
    const element: HTMLElement = fixture.nativeElement;
    const createdListLength = element.getElementsByTagName('li').length;
    expect(optionsLength).toBe(createdListLength);
  });

  it('should hide list on load', () => {
    expect($(fixture.nativeElement).find('.dropdown-options').eq(0).hasClass('d-none')).toBe(true);
  });

  it('should toggle when clicked on dropdown', () => {
    $(fixture.nativeElement).find('.dropdown').click();
    expect($(fixture.nativeElement).find('.dropdown-options').hasClass('d-none')).toBe(false);
    $(fixture.nativeElement).find('.dropdown').click();
    expect($(fixture.nativeElement).find('.dropdown-options').hasClass('d-none')).toBe(true);
  });

  it('should open when clicked on dropdoen and hide when clicked on body', () => {
    $(fixture.nativeElement).find('.dropdown').click();
    expect($(fixture.nativeElement).find('.dropdown-options').hasClass('d-none')).toBe(false);
    $('body').click();
    expect($(fixture.nativeElement).find('.dropdown-options').hasClass('d-none')).toBe(true);
  });
});
