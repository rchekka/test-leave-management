import { Component, OnInit, AfterViewInit, ElementRef, HostListener, Input, Output,  ViewChild, EventEmitter} from '@angular/core';
import { Server } from 'selenium-webdriver/safari';

@Component({
    selector: 'app-dropdown',
    templateUrl: './dropdown.component.html',
    styleUrls: ['./dropdown.component.css']
})

export class DropdownComponent implements OnInit, AfterViewInit {
    @ViewChild('list') list: ElementRef;
    @Input() ddValue: any;
    @Input() ddOptions: any;
    @Output() ddValueChange = new EventEmitter<any>();

    constructor(private element: ElementRef) { }

    ngOnInit() { }

    ngAfterViewInit() {
        this.list.nativeElement.classList.add('d-none');
    }

    /*
     * Name: document:click
     * Author: Chaitanya
     * parameters: Nothing
     * Return : Nothing
     * Process: It toggles the dropdown option when user clicks on dropdown and hides when user clicks on body.
    */
    @HostListener('document:click') clickout() {
        if (this.element.nativeElement.contains(event.target)) {
            this.list.nativeElement.classList.toggle('d-none');
        } else {
            this.list.nativeElement.classList.add('d-none');
        }
    }

    // YCR: Define the type the paremeter you are accepting. like index: number, option: ElementRef

    /*
     * Name: selectDropdownValue
     * Author: Chaitanya
     * parameters: 2 (selected list index)
     * Return : Nothing
     * Process: It changes the dropdown value and emits the updated value to parent component.
    */
    selectDropdownValue(index: number) {
        this.ddValue = this.ddOptions[index].value;
        this.ddValueChange.emit(this.ddValue);
    }

}
