import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { DebugElement } from '@angular/core';

import { FooterComponent } from './footer.component';

describe('FooterComponent', () => {
  let component: FooterComponent;
  let fixture: ComponentFixture<FooterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FooterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FooterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });
  it('checks for the padding of the footer', function() {
    expect(window.getComputedStyle(document.getElementsByClassName('footer')[0]).backgroundColor).toEqual('rgb(250, 250, 250)');
  });

  it('checks for the padding of the footer', function() {
    expect(window.getComputedStyle(document.getElementsByClassName('footer')[0]).padding).toEqual('12px 32px');
  });
});
