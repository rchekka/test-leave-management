import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LeavesConfigurationComponent } from './leaves-configuration.component';

describe('LeavesConfigurationComponent', () => {
  let component: LeavesConfigurationComponent;
  let fixture: ComponentFixture<LeavesConfigurationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LeavesConfigurationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LeavesConfigurationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
