import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployeesOnLeaveComponent } from './employees-on-leave.component';
import { AppModule } from '../../../app.module';

describe('EmployeesOnLeaveComponent', () => {
  let component: EmployeesOnLeaveComponent;
  let fixture: ComponentFixture<EmployeesOnLeaveComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [],
      providers: [
        EmployeesOnLeaveComponent
      ],
      imports: [AppModule]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployeesOnLeaveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });


  it('Checks if array is the data type', () => {
    expect(component.employeeData).toEqual(jasmine.any(Array));
  });

  it('Checks if default array is empty', () => {
    expect(component.employeeData.length).toEqual(0);
  });

  xit('checks tables values are binding from html', () => {
    // expect(component.employeeData.length).toEqual(0);
  });

});
