import { Component, OnInit } from '@angular/core';

import { EmployeesOnLeaveDataTypes } from '../../../employees-on-leave-data-types';
import { EmployeesOnLeaveDataService } from '../../../services/employees-on-leave.service';
import { Constants } from '../../../constants';

@Component({
  selector: 'app-employees-on-leave',
  templateUrl: './employees-on-leave.component.html',
  styleUrls: ['./employees-on-leave.component.css']
})

export class EmployeesOnLeaveComponent implements OnInit {
  employeeData: EmployeesOnLeaveDataTypes[] = [];
  oneDay: number;
  halfDay: number;

  constructor(private employeesOnLeaveService: EmployeesOnLeaveDataService) { }

  ngOnInit() {
    this.oneDay = Constants.ONE_DAY;
    this.halfDay = Constants.HALF_DAY;
    this.getEmployeesOnLeave();
  }

  /*
    * Name: getEmployeesOnLeave();
    * Author: Rajesh
    * parameters: Nothing
    * Return : Gets the data of employees on leave
    * Process: This function is used to get the employees on leave data from backend.
        This function uses the 'employeesOnLeaveService' service to connect the backend and get the data;
  */
  getEmployeesOnLeave() {
    this.employeesOnLeaveService.getEmployeesOnLeaveList().subscribe(
      (success) => {
        this.employeeData = success;
      },
      (error) => {
        // Error callback to be defined as there is no design for it.
      }
    );
  }
}
