export class EmployeesOnLeaveDataTypes {
    leaveStartDate: string;
    leaveEndDate: string;
    name: string;
    duration: number;
    leaveStartDay: string;
    leaveEndDay: string;
}
