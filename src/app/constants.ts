export class Constants {
    public static EMAIL = 'rchekka@uxreactor.com';
    public static MONTH_NAMES = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'June', 'July', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
    public static LEAVE_TYPE = 'privilege';
    public static USER_TYPE = 'member';
    static DROPDOWN_DEFAULTVALUE = 'Upcoming Holidays';
    static DROPDOWN_OPTIONS = ['Upcoming Holidays', 'Past Holidays'];
    static ORGANISATION = 'Organisation';
    static MY_LEAVES = 'My Leaves';
    static ERROR_MESSAGE = 'Sorry You Have An Error';
    static CHART_TYPE = 'column';
    static TRANSPARENT = 'transparent';
    static GRAPH_ID = 'org-graph';
    static GREEN = '#4CAF50';
    static YELLOW = '#FFC107';
    static BLACK = '#616161';
    static LEFT = 'left';
    static HORIZONTAL = 'horizontal';
    static BOTTOM = 'bottom';
    static ZERO = 0;
    static FALSE = false;
    static TRUE = true;
    static TEN = 10;
    static FIVE = 5;
    static ONE = 1;
    static TWO = 2;
    static PREVILEGED = 'Privilege Leave';
    static SICK = 'Sick Leave';
    static SPECIAL = 'Special Leave';
    static DATE_FORMAT = 'dd mmm';
    static TODAY = 'Today';
    static START_WEEK = 'mo';
    static EMPTY_STRING = '';
    static NULL = null;
    static THOUSAND = 1000;
    static TWENTY = 20;
    static GRAPH_WIDTH = 270;
    static LEGEND_COLOUR = '#464B56';
    static LEGEND_SIZE = '10px';
    static LEGEND_WEIGHT = 'lighter';
    static ADMIN_ROLE = 'admin';
    static MEMBER_ROLE = 'member';

    // tslint:disable-next-line:max-line-length
    public static MONTHS = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
    public static DAYS = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];


    public static NO_OF_LEAVES = 1;
    public static SICK_LEAVE_BALANCE = 2;
    public static SPECIAL_LEAVE_BALANCE = 1;
    public static PRIVILEGE_LEAVE_BALANCE = 4;
    public static LEAVES_TAKEN = 4;
    public static INFORMER = 'Raj Gopal Prasad';
    public static NOTIFIER = '';
    public static LIST_OF_INFORMERS = [{
        value: 'Vijaya Prabhakar Konduri',
    },
    {
        value: 'Chaitanya Kunduru'
    },
    {
        value: 'BalaNarsimha Rao Tanneeru'
    },
    {
        value: 'Raj Gopal Prasad'
    }];

    public static LIST_OF_NOTIFIERS = [{
        name: 'Vijaya Prabhakar Konduri',
        email: 'vkonduri@uxreactor.com'
    },
    {
        name: 'Chaitanya Kunduru',
        email: 'ckunduru@uxreactor.com'
    },
    {
        name: 'BalaNarsimha Rao Tanneeru',
        email: 'btanneeru@uxreactor.com'
    },
    {
        name: 'Raj Gopal Prasad',
        email: 'kprasad@uxreactor.com'
    }];
    // Datepicker Constants
    public static ONE_DAY_TO_MILLI_SECONDS_FORMULA = 1000 * 60 * 60 * 24;
    public static CEO_USER_TYPE = 'ceo';
    public static MEMBER_USER_TYPE = 'member';
    public static PRIVILEGE_LEAVE_TYPE = 'privilege';
    public static SICK_LEAVE_TYPE = 'sick';
    public static SPECIAL_LEAVE_TYPE = 'special';
    public static AM = 'am';
    public static PM = 'pm';
    public static ONE_DAY = 1;
    public static HALF_DAY = 0.5;

}
