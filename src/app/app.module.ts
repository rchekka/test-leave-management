import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { APP_BASE_HREF } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FooterComponent } from './components/shared/footer/footer.component';
import { HeaderComponent } from './components/shared/header/header.component';
import { AttendanceComponent } from './components/attendance/attendance.component';
import { EmployeesComponent } from './components/employees/employees.component';
import { OrganizationComponent } from './components/organization/organization.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { EmployeesOnLeaveComponent } from './components/dashboard/employees-on-leave/employees-on-leave.component';
import { EmployeesOnLeaveDataService } from './services/employees-on-leave.service';
import { RequestLeaveModelComponent } from './components/shared/request-leave-model/request-leave-model.component';

import { DropdownComponent } from './components/shared/dropdown/dropdown.component';
import { SearchSuggestionsComponent } from './components/shared/search-suggestions/search-suggestions.component';
import { MyDatePickerModule } from 'angular4-datepicker/src/my-date-picker';
import { RequestLeaveService } from './services/request-leave.service';
import { SetupComponent } from './components/setup/setup.component';
import { LeavesConfigurationComponent } from './components/setup/leaves-configuration/leaves-configuration.component';


@NgModule({
  declarations: [
    AppComponent,
    FooterComponent,
    HeaderComponent,
    AttendanceComponent,
    EmployeesComponent,
    OrganizationComponent,
    DashboardComponent,
    EmployeesOnLeaveComponent,
    RequestLeaveModelComponent,
    DropdownComponent,
    SearchSuggestionsComponent,
    SetupComponent,
    LeavesConfigurationComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpModule,
    FormsModule,
    ReactiveFormsModule,
    MyDatePickerModule
  ],
  providers: [
    EmployeesOnLeaveDataService,
    RequestLeaveService,
    { provide: APP_BASE_HREF, useValue: '/' }
  ],
  bootstrap: [
    AppComponent
  ]
})
export class AppModule { }
