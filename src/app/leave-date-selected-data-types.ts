export class LeaveDatesFormat {
    year: number;
    month: number;
    day: number;
}
