import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Http, Response } from '@angular/http';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class RequestLeaveService {

  constructor(private http: Http) { }

  /*
    * Name: requestEmployeeLeave(requestLeaveData)
    * Author: Rajesh
    * parameters: requestLeaveData
    * Return : Backend Response JSON
    * Process: This function is used to send the requested leave data to backend.
  */
  requestEmployeeLeave(requestLeaveData) {
    console.log(requestLeaveData);
    return this.http.post('http://192.168.27.139/leave-management-server/controllers/request_employee_leave.php', requestLeaveData).map(
      (response: Response) => {
        return response.json();
      }
    ).catch((error: Response) => {
      return Observable.throw('errorMessage');
    });
  }
}
