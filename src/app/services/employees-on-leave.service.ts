import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Http, Response } from '@angular/http';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import { EmployeesOnLeaveDataTypes } from '../employees-on-leave-data-types';

@Injectable()
export class EmployeesOnLeaveDataService {
  constructor(private http: Http) { }

  getEmployeesOnLeaveList() {
    return this.http.get('http://192.168.27.139/leave-management-server/controllers/get_employees_on_leave.php')
    .map(
      (response: Response) => {
        return response.json();
      }
    )
    .catch((error: Response) => {
      let errorMessage;
      if (error.status === 400) {
        errorMessage = '400: Bad Request';
      } else if (error.status === 404) {
        errorMessage = '404: Not Found';
      } else if (error.status === 500) {
        errorMessage = '500: Internal Server Error';
      }
      return Observable.throw(errorMessage);
    });
  }
}
