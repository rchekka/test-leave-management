import { ReflectiveInjector } from '@angular/core';
import { async, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { BaseRequestOptions, ConnectionBackend, Http, RequestOptions } from '@angular/http';
import { Response, ResponseOptions } from '@angular/http';
import { MockBackend } from '@angular/http/testing';

import { RequestLeaveService } from './request-leave.service';
import { AppModule } from '../app.module';

describe('RequestLeaveService', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [],
      imports: [ AppModule]
    })
    .compileComponents();
  }));
  beforeEach(() => {
    this.injector = ReflectiveInjector.resolveAndCreate([
      {provide: ConnectionBackend, useClass: MockBackend},
      {provide: RequestOptions, useClass: BaseRequestOptions},
      Http,
      RequestLeaveService
    ]);
    this.requestLeaveService = this.injector.get(RequestLeaveService);
    this.backend = this.injector.get(ConnectionBackend) as MockBackend;
    this.backend.connections.subscribe((connection: any) => this.lastConnection = connection);
  });

  it('should be created', () => {
    expect(this.requestLeaveService).toBeTruthy();
  });

  it('requestEmployeeLeave() should query current service url', () => {
    this.requestLeaveService.requestEmployeeLeave();
    expect(this.lastConnection).toBeDefined('no http service connection at all?');
    expect(this.lastConnection.request.url).toMatch('http://192.168.27.139/leave-management-server/controllers/request_employee_leave.php');
  });

  it('requestEmployeeLeave() should return some heroes', fakeAsync(() => {
    const mockResponse = [{
      'email': 'btanneeru@uxreactor.com'
    }];
    let result: any;
    this.requestLeaveService.requestEmployeeLeave().subscribe((response) => result = response);
    this.lastConnection.mockRespond(new Response(new ResponseOptions({
      body: mockResponse,
    })));
    tick();
    expect(result.length).toEqual(1);
    expect(result[0].email).toEqual('btanneeru@uxreactor.com');
  }));

  it('requestEmployeeLeave() while server is down', fakeAsync(() => {
    let catchedError;
    this.requestLeaveService.requestEmployeeLeave().subscribe(
      (error) => {
        catchedError = error;
      }
    );
    this.lastConnection.mockRespond(new Response(new ResponseOptions({
      status: 404,
      statusText: 'URL not Found',
    })));
    tick();
    expect(catchedError).toBeDefined();
  }));
});
