import {Injectable, Injector, ReflectiveInjector} from '@angular/core';
import {async, fakeAsync, tick} from '@angular/core/testing';
import {BaseRequestOptions, ConnectionBackend, Http, RequestOptions} from '@angular/http';
import {Response, ResponseOptions} from '@angular/http';
import {MockBackend, MockConnection} from '@angular/http/testing';

import { AppModule } from '../app.module';

import { EmployeesOnLeaveDataService } from './employees-on-leave.service';

describe('EmployeesOnLeaveDataService', () => {

  beforeEach(() => {
    this.injector = ReflectiveInjector.resolveAndCreate([
      {provide: ConnectionBackend, useClass: MockBackend},
      {provide: RequestOptions, useClass: BaseRequestOptions},
      Http,
      EmployeesOnLeaveDataService
    ]);
    this.EmployeesOnLeaveDataService = this.injector.get(EmployeesOnLeaveDataService);
    this.backend = this.injector.get(ConnectionBackend) as MockBackend;
    this.backend.connections.subscribe((connection: any) => this.lastConnection = connection);
  });


  it('should be created EmployeesOnLeaveDataService', () => {
    expect(this.EmployeesOnLeaveDataService).toBeTruthy();
  });

  it('getEmployeesOnLeaveList() should query current service url', () => {
    this.EmployeesOnLeaveDataService.getEmployeesOnLeaveList();
    expect(this.lastConnection).toBeDefined('no http service connection at all?');
    expect(this.lastConnection.request.url).toMatch('http://192.168.27.139/leave-management-server/controllers/get_employees_on_leave.php');
  });


  it('getEmployeesOnLeaveList() while server is down', fakeAsync(() => {
    let catchedError;
    this.EmployeesOnLeaveDataService.getEmployeesOnLeaveList().subscribe(
      (error) => {
        catchedError = error;
      }
    );
    this.lastConnection.mockRespond(new Response(new ResponseOptions({
      status: 404,
      statusText: 'URL not Found',
    })));
    tick();
    expect(catchedError).toBeDefined();
  }));

});
