import { AppPage } from './app.po';
import {browser, by, element} from 'protractor';

describe('leave-management-angular5 App', () => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();
  });
  it('should display footer text', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Copyright. 2018 UXReactor. All Rights Reserved.');
  });

  it('should exist footer logo path', () => {
    page.navigateTo();
    expect(page.getFooterLogoPath()).toBeTruthy();
  });

  it('should exist header logo path', () => {
    page.navigateTo();
    expect(page.getHeaderLogoPath()).toBeTruthy();
  });

  it('should display dashboard text', () => {
    page.navigateTo();
    expect(page.getHeaderText()).toEqual('Dashboard');
  });
});

