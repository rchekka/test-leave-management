import { browser, by, element } from 'protractor';

export class AppPage {
  navigateTo() {
    return browser.get('./src/');
  }
  getParagraphText() {
    return element(by.css('app-footer p')).getText();
  }

  getFooterLogoPath() {
    return expect(element(by.binding('../../assets/three-dollars-footer-logo.png')));
  }
  getHeaderLogoPath() {
    return expect(element(by.binding('../../assets/leave-management-header-logo.png')));
  }
  getHeaderText() {
    return element(by.css('ul li:first-child')).getText();
  }
}
